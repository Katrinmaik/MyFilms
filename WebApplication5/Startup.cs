﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using WebApplication5.Models;
using Swashbuckle.AspNetCore.Swagger;

namespace WebApplication5
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddDbContext<WebApplication5Context>(options =>
                options.UseInMemoryDatabase(Guid.NewGuid().ToString()));

            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1", new Info());

            });

            services.AddDbContext<WebApplication5Context>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("WebApplication5Context")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.DocumentTitle=Assembly.GetExecutingAssembly().GetName().Name;
                options.ShowExtensions();

                options.DefaultModelRendering(ModelRendering.Example);
                
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Films API V1");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Defolt}/{action=Index}/{id?}"
                );
            });
        }
    }
}
